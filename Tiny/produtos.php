<?php

namespace Tiny;

require_once('api.php');

/**
 * Produtos
 * https://tiny.com.br/info/api.php?p=api2-produtos-pesquisar
 */
class Produtos extends API {
    /**
     * @var string $MODULE
     * @var string $CONTAINER
     * @var string $UPDATE_VERB
     */
    protected static $MODULE = 'produto';
    protected static $CONTAINER = 'produtos';
}