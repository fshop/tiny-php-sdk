<?php

namespace Tiny;

/**
 * SDK para API v2 da ferramenta de gestão Tiny
 * https://tiny.com.br/info/api-desenvolvedores?tab=api2
 *
 * este projeto foi baseado no projeto PHP-SDK do MercadoLivre
 * https://github.com/mercadolibre/php-sdk/
 *
 * @author Ricardo Santos <rsantos@gmail.com>
 * @version 0.0.1
 */
require_once('api.php');
require_once('pedidos.php');
require_once('produtos.php');
