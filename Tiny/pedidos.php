<?php

namespace Tiny;

require_once('api.php');

/**
 * Pedidos
 * https://tiny.com.br/info/api.php?p=api2-pedidos-pesquisar
 */
class Pedidos extends API {
    /**
     * @var string $MODULE
     * @var string $CONTAINER
     * @var string $UPDATE_VERB
     */
    protected static $MODULE = 'pedido';
    protected static $CONTAINER = 'pedidos';
}