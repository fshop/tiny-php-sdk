<?php

namespace Tiny;

/**
 * API (Core)
 * https://tiny.com.br/info/api-desenvolvedores?tab=api2
 */
abstract class API {
    /**
     * @var $apikey
     * @var $MODULE
     * @var $API_ROOT_URL
     * @var $UPDATE_VERB
     * @var $CURL_OPTS
     */
    protected $token;
    protected static $MODULE = 'null';
    protected static $CONTAINER = 'null';
    protected static $API_ROOT_URL = 'https://tiny.com.br/api2';
    protected static $CURL_OPTS = array(
        CURLOPT_USERAGENT => 'TINY-PHP-SDK-0.0.1',
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_TIMEOUT => 60,
    );

    /**
     * @param string $token
     */
    public function __construct($token) {
        $this->token = $token;
    }

    /**
     * executa todas as chamadas
     *
     * @param string $path
     * @param array $params
     * @param array $opts
     * @return array
     */
    public function _execute($path, $params = array(), $opts = array()) {
        $uri = $this->_make_path($path, $params);
        print $uri."\n";

        $ch = curl_init($uri);
        curl_setopt_array($ch, self::$CURL_OPTS);
        if(!empty($opts))
            curl_setopt_array($ch, $opts);

        $results = json_decode(curl_exec($ch), true);
        if(isset($results['retorno'])) {
            $results = $results['retorno'];
        }

        $results = array_merge(($results ? $results : array()), array(
            'path' => $path,
            'uri' => $uri,
            'httpcode' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        ));
        curl_close($ch);

        return $results;
    }

    /**
     * monta a URL completa para chamada
     *
     * @param string $path
     * @param array $params
     * @return string
     */
    public function _make_path($path, $params = null) {
        if(!preg_match('/^http/', $path)) {
            if (!preg_match('/^\//', $path)) {
                $path = '/'.$path;
            }
            if (!preg_match('/\.php$/', $path)) {
                $path .= '.php';
            }
            $uri = self::$API_ROOT_URL.$path;
            if($params) {
                $uri .= '?'.http_build_query($params);
            }
        } else {
            $uri = $path;
        }

        return $uri;
    }

    /**
     * executa um POST
     *
     * @param string $path
     * @param array $params
     * @param array $data
     * @param string $verb
     * @return mixed
     */
    public function _post($path, $params = array(), $data = array(), $verb = 'POST') {
        $data['formato'] = 'json';
        $data['token'] = $this->token;

        $opts = array(
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_CUSTOMREQUEST => $verb,
        );
        return $this->_execute($path, $params, $opts);
    }

    /**
     * pesquisar registros
     *
     * @param array $data
     * @return mixed
     */
    public function pesquisar($data) {
        $path = static::$CONTAINER.'.pesquisa';
        return $this->_post($path, null, $data);
    }

    /**
     * atualizar registro
     *
     * @param array $itemdata
     * @return mixed
     */
    public function alterar($itemdata) {
        return $this->incluir($itemdata, 'alterar');
    }

    /**
     * incluir registro
     *
     * @param array $itemdata
     * @param string $acao
     * @return mixed
     */
    public function incluir($itemdata, $acao = 'incluir') {
        $module = static::$MODULE;
        $container = static::$CONTAINER;
        $path = $module.'.'.$acao;

        if(!isset($itemdata[0])) {
            $itemdata = array($itemdata);
        }

        $data = array($container => array());
        foreach($itemdata as $item) {
            $data[$container][] = array($module => $item);
        }
        $data = array(
            $module => json_encode($data),
        );

        return $this->_post($path, null, $data);
    }

    /**
     * obter registro
     *
     * @param string $id
     * @return mixed
     */
    public function obter($id) {
        $path = static::$MODULE.'.obter';
        $data = array(
            'id' => $id,
        );
        return $this->_post($path, null, $data);
    }
}