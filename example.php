<?php

require('Tiny/tiny.php');

$token = 'SeuTokenAqui';
$tiny_produtos = new \Tiny\Produtos($token);

$data = array(
    array(
        'sequencia' => '1234',
        'codigo' => '1234',
        'nome' => 'Nome Teste',
        'unidade' => 'UN',
        'preco' => '123.45',
        'origem' => '0',
        'situacao' => 'A',
        'tipo' => 'P',
    ),
    array(
        'sequencia' => '12345',
        'codigo' => '12345',
        'nome' => 'Nome Teste2',
        'unidade' => 'UN',
        'preco' => '123.46',
        'origem' => '0',
        'situacao' => 'A',
        'tipo' => 'P',
    ),
);

$results = $tiny_produtos->incluir($data);
print_r($results);
